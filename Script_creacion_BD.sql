CREATE DATABASE  IF NOT EXISTS `java_curso` /*!40100 DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish2_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `java_curso`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: java_curso
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorias` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8mb3_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'Ordenadores'),(2,'Impresoras'),(3,'Libros'),(4,'Películas'),(5,'Hogar'),(6,'Complementos'),(7,'Zapatos'),(8,'Mobiliario'),(9,'Maquinaria'),(10,'Herramientas');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8mb3_spanish2_ci DEFAULT NULL,
  `idCategoria` bigint DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  `sku` varchar(45) COLLATE utf8mb3_spanish2_ci DEFAULT NULL,
  `fecha_registro` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prod_cat_idx` (`idCategoria`),
  CONSTRAINT `fk_prod_cat` FOREIGN KEY (`idCategoria`) REFERENCES `categorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Rozamond',2,57,'49338','2022-02-13'),(2,'Lillian',4,10,'24591','2022-07-20'),(3,'Courtenay',4,265,'30963','2022-06-30'),(4,'Syman',6,234,'95084','2023-01-17'),(5,'Constancy',10,112,'95614','2022-08-16'),(6,'Myron',10,88,'62842','2022-05-04'),(7,'Laurette',2,258,'72005','2022-06-10'),(8,'Ruthi',2,192,'92061','2022-12-09'),(9,'Justino',7,103,'16247','2022-08-09'),(10,'Jorrie',9,11,'30736','2022-06-09'),(11,'Portie',2,88,'76060','2022-11-14'),(12,'Elnora',9,20,'56992','2022-02-21'),(13,'Rodrick',1,253,'53335','2022-08-11'),(14,'Elizabet',8,210,'58674','2022-11-28'),(15,'Margaretta',10,153,'63454','2022-11-09'),(16,'Adrea',9,293,'56832','2022-06-08'),(17,'Viv',10,286,'63450','2022-02-01'),(18,'Shellie',6,287,'87506','2022-08-20'),(19,'Lief',10,95,'22341','2022-07-23'),(20,'Vidovic',4,288,'97750','2022-11-08'),(21,'Krista',2,237,'52924','2022-05-13'),(22,'Darice',3,161,'79491','2022-03-25'),(23,'Sara-ann',5,258,'50676','2022-03-25'),(24,'Annmaria',2,256,'51630','2022-07-18'),(25,'Olivero',8,224,'43002','2022-04-10'),(26,'Dasi',6,140,'56780','2022-02-08'),(27,'Angelika',3,239,'95309','2022-07-24'),(28,'Yorke',2,290,'55330','2022-10-12'),(29,'Kaja',4,297,'38044','2022-11-17'),(30,'Bev',7,253,'11729','2022-02-15'),(31,'Odetta',8,226,'49407','2022-02-02'),(32,'Colette',10,59,'40788','2022-09-09'),(33,'Natividad',8,201,'67348','2022-09-17'),(34,'Adeline',10,34,'74579','2022-03-02'),(35,'Cristine',10,50,'17505','2022-08-19'),(36,'Egbert',8,154,'16955','2022-03-31'),(37,'Audrie',10,204,'75226','2022-05-20'),(38,'Pet',5,261,'21305','2022-02-18'),(39,'Marylin',8,142,'79606','2022-07-22'),(40,'Erina',3,104,'48914','2022-03-17'),(41,'Rustie',9,134,'74779','2022-04-20'),(42,'Meghan',2,155,'84190','2022-12-19'),(43,'Meryl',8,290,'33309','2022-09-02'),(44,'Candida',6,269,'61499','2022-06-03'),(45,'Julieta',1,228,'46832','2022-07-04'),(46,'Bobby',3,199,'90706','2023-01-27'),(47,'Taddeusz',4,47,'43012','2022-09-30'),(48,'Silvie',5,129,'52163','2022-11-07'),(49,'Xever',8,120,'58010','2022-02-17'),(50,'Sam',4,54,'47201','2022-08-18');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) COLLATE utf8mb3_spanish2_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8mb3_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(45) COLLATE utf8mb3_spanish2_ci DEFAULT NULL,
  `clave` varchar(100) COLLATE utf8mb3_spanish2_ci NOT NULL,
  `perfil` varchar(45) COLLATE utf8mb3_spanish2_ci DEFAULT 'usuario',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin','Carlos','Ponce de León','12345','admin'),(2,'carlos','Carlos','López','9876','usuario'),(3,'luperez','Luisa','Pérez','4567','usuario'),(4,'rogon','Rosa','González','3214','usuario'),(5,'jogar','José','García','741','usuario'),(6,'PatiP','Patricia','Ponce de León','6543','usuario'),(7,'CarlotaP','Carlota','Ponce de León','369','usuario'),(8,'MariaP',NULL,NULL,'258',NULL),(9,'rope',NULL,NULL,'741',NULL),(14,'Carp','Carpetano','Rodriguez','123','conductor');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-01 12:56:39
