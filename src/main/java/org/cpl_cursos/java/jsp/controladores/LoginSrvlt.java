package org.cpl_cursos.java.jsp.controladores;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.cpl_cursos.java.jsp.servicios.LoginSrvc;
import org.cpl_cursos.java.jsp.servicios.LoginSrvcImpl;
import org.cpl_cursos.java.jsp.servicios.LoginSrvcSessionImpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Optional;

@WebServlet("/login")
public class LoginSrvlt extends HttpServlet {
    // creamos dos constantes para cmrpobar usuario y clave.
    // En la versión final estos datos se obtienen de la BBDD
    final static String USERNAME = "admin";
    final static String CLAVE = "12345";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //LoginSrvc auth = new LoginSrvcImpl();
        //Optional<String> cookieOpt = auth.getUsuario(req);

        // En esta rama -4_login_session- utilizamos una sesión
        LoginSrvc auth = new LoginSrvcSessionImpl();
        Optional<String> usu = auth.getUsuario(req);
        //System.out.println("Datos sesisón: " +usu.get());
        // Si hay una cookie devolvemos un saludo al admin
        if(usu.isPresent()) {
            resp.setContentType("text/html");
            try (PrintWriter out = resp.getWriter()) {

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("    <head>");
                out.println("        <meta charset=\"UTF-8\">");
                out.println("        <title>Hola " + usu.get() + "</title>");
                out.println("    </head>");
                out.println("    <body>");
                out.println("        <h1>Hola " + usu.get() + " ya has iniciado sesión.</h1>");
                out.println("<div><a href='" + req.getContextPath() + "/index.html'>Volver</a></div>");
                if(usu.isPresent()){
                    out.println("<div><a href='" + req.getContextPath() + "/salir'>Salir</a></div>");
                }
                out.println("    </body>");
                out.println("</html>");
            }
        } else {
                // si no hay cookie, cargamos el formulario de login
                getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    // El formulario envía un método POST
    @Override
    protected void  doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //  Extraemos los datos del formulario (llegan como parámetros en el cuerpo del mismo)
        String username = req.getParameter("username");
        String clave = req.getParameter("password");

        // Si coincide usuario y clave, devolvemos un mensaje de éxito
        if (USERNAME.equals(username) && CLAVE.equals(clave)) {

            // Como estamos en el post, creamos y añadimos la cookie de usuario
            // Cookie userCk = new Cookie("username", username);
            // resp.addCookie(userCk);

            // En esta rama -4_login_session- utilizamos una sesión
            HttpSession sesion = req.getSession();
            sesion.setAttribute("username",username);

            resp.setContentType("text/html");
            try (PrintWriter out = resp.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("    <head>");
                out.println("        <meta charset=\"UTF-8\">");
                out.println("        <title>Login correcto</title>");
                out.println("    </head>");
                out.println("    <body>");
                out.println("        <h1>Login correcto!</h1>");
                out.println("        <h3>Hola "+ username + " has iniciado sesión con éxito!</h3>");
                out.println("<div><a href='" + req.getContextPath() + "/index.html'>Volver</a></div>");
                out.println("    </body>");
                out.println("</html>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // Si no coinciden usuario y/o clave, enviamos un resultado de error del usuario (4xx)
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error. Usuario y/o contraseña no válidos.");
        }
    }
}
