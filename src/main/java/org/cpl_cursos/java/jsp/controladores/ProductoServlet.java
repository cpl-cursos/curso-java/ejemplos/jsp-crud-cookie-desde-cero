package org.cpl_cursos.java.jsp.controladores;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.cpl_cursos.java.jsp.modelos.Producto;
import org.cpl_cursos.java.jsp.servicios.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet("/listaprod")
public class ProductoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        // Pasamos al service la conexión que recibimos en req (creada por el ConexionFiltro)
        // req.Atribute devuelve una clase Object, por eso hay que envolver el resultado como un objeto de la clase Connection
        ProducotSrvc prodservice = new ProductoSrvcImpl((Connection) req.getAttribute("conn"));

        List<Producto> productos = prodservice.listaProd();

        LoginSrvc auth = new LoginSrvcSessionImpl();
        Optional<String> usu = auth.getUsuario(req);

        req.setAttribute("productos", productos);
        req.setAttribute("username", usu);
        getServletContext().getRequestDispatcher("/listaProductos.jsp").forward(req, resp);
    }
}
