package org.cpl_cursos.java.jsp.controladores;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.cpl_cursos.java.jsp.servicios.LoginSrvc;
import org.cpl_cursos.java.jsp.servicios.LoginSrvcImpl;
import org.cpl_cursos.java.jsp.servicios.LoginSrvcSessionImpl;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/salir")
public class LogoutSrvltl extends HttpServlet {
    @Override
    protected void doGet (HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LoginSrvc auth = new LoginSrvcSessionImpl();
        /*Optional<String> usu = auth.getUsuario(req);
        if(usu.isPresent()) {
            Cookie usuCk = new Cookie("username", "");
            usuCk.setMaxAge(0);
            resp.addCookie(usuCk);
        }*/

        // En esta rama -4_login_session- utilizamos una sesión
        Optional<String> usu= auth.getUsuario(req);
        if(usu.isPresent()){
            HttpSession sesion = req.getSession();
            sesion.invalidate();
        }

        resp.sendRedirect(req.getContextPath() + "/login");
    }
}
