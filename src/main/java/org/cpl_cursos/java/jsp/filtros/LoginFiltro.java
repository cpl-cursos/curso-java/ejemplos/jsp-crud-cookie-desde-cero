package org.cpl_cursos.java.jsp.filtros;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.cpl_cursos.java.jsp.servicios.LoginSrvc;
import org.cpl_cursos.java.jsp.servicios.LoginSrvcSessionImpl;

import java.io.IOException;
import java.util.Optional;

@WebFilter("/listaprod")
public class LoginFiltro implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LoginSrvc srvc = new LoginSrvcSessionImpl();
        Optional<String> username = srvc.getUsuario((HttpServletRequest) request);
        if(username.isPresent()) {
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse)response).sendError(HttpServletResponse.SC_UNAUTHORIZED,
                    "Lo sentimos, no tiene acceso a este contenido.");
        }
    }
}
