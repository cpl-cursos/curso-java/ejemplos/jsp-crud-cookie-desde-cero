package org.cpl_cursos.java.jsp.servicios;

import org.cpl_cursos.java.jsp.modelos.Producto;

import java.util.List;
import java.util.Optional;

public interface ProducotSrvc {
    List<Producto> listaProd();
    Optional<Producto> porId(Long id);
    void guardar(Producto prod);
    void borrar(Long id);
}
