package org.cpl_cursos.java.jsp.servicios;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import java.util.Optional;

public class LoginSrvcSessionImpl implements LoginSrvc{

    @Override
    public Optional<String> getUsuario(HttpServletRequest req) {
        HttpSession sesion = req.getSession();
        String usu = (String) sesion.getAttribute("username");
        if(usu != null) {
            return Optional.of(usu);
        }
        return Optional.empty();
    }
}
