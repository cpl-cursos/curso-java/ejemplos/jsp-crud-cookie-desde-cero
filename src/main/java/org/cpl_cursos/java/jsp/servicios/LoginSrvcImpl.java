package org.cpl_cursos.java.jsp.servicios;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.Optional;

public class LoginSrvcImpl implements LoginSrvc{

    @Override
    public Optional<String> getUsuario(HttpServletRequest req) {
        // Obtenemos las cookie. Com pueden ser nulas, hacemos una comprobación con el operador ternario.
        Cookie[] cookies = req.getCookies() != null ? req.getCookies() : new Cookie[0];
        // vamos a buscar si existe la cookie de usuario
        return Arrays.stream(cookies)
                .filter(c -> "username".equals(c.getName()))  // buscamos la que se lllama "username" (se fijo el nombre en el doPost)
                .map(Cookie::getValue) // "mapea" a una cadena. Equivale a .map(c -> c.getValue()), que a su vez equivale a .map(c -> {return c.getValue();})
                .findAny(); // Si hay alguna, la devuelve.
    }
}
