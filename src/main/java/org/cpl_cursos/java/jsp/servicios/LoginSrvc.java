package org.cpl_cursos.java.jsp.servicios;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Optional;

public interface LoginSrvc {
    Optional<String> getUsuario(HttpServletRequest req);
}
