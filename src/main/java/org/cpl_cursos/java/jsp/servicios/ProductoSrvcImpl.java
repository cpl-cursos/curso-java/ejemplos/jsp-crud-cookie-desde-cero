package org.cpl_cursos.java.jsp.servicios;

import org.cpl_cursos.java.jsp.modelos.Producto;
import org.cpl_cursos.java.jsp.repositorios.ProductoRepoImpl;
import org.cpl_cursos.java.jsp.repositorios.Repositorio;
import org.cpl_cursos.java.jsp.utilidades.ConexionBD;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProductoSrvcImpl implements ProducotSrvc{
    private Repositorio<Producto> repoProd;

    public ProductoSrvcImpl(Connection conexion) {
        this.repoProd = new ProductoRepoImpl(conexion);
    }

    @Override
    public List<Producto> listaProd() {
        return repoProd.listar();
    }

    @Override
    public Optional<Producto> porId(Long id) {
        return Optional.ofNullable(repoProd.porId(id));
    }

    @Override
    public void guardar(Producto prod) {

    }

    @Override
    public void borrar(Long id) {

    }
}
