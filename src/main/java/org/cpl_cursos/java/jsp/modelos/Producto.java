package org.cpl_cursos.java.jsp.modelos;

import java.time.LocalDate;

public class Producto {
    private Long id;
    private String nombre;
    private Long idCategoria;
    private Double precio;
    private String sku;
    private LocalDate fecha_registro;

    public Producto() {
    }

    public Producto(Long id, String nombre, Long idCat, Double precio) {
        this.id = id;
        this.nombre = nombre;
        this.idCategoria = idCat;
        this.precio = precio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public LocalDate getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(LocalDate fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
}
