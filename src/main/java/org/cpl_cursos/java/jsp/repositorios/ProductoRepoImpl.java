package org.cpl_cursos.java.jsp.repositorios;

import org.cpl_cursos.java.jsp.modelos.Producto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductoRepoImpl implements Repositorio<Producto> {
    private Connection conn;

    public ProductoRepoImpl(Connection conexion) {
        this.conn = conexion;
    }

    @Override
    public List<Producto> listar() {
        List<Producto> productos = new ArrayList<>();

        try(Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM productos")) {
            while (rs.next()) {
                Producto p = getProducto(rs);
                productos.add(p);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return productos;
    }

    @Override
    public Producto porId(Long id) {
        Producto prod = null;
        try(PreparedStatement stmt = conn.prepareStatement("SELECT * FROM productos WHERE id=?")) {
            stmt.setLong(1, id);

            try(ResultSet rs = stmt.executeQuery()) {
                if(rs.next()) {
                    prod = getProducto(rs);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return prod;
    }

    @Override
    public void guardar(Producto producto) {

    }

    @Override
    public void borrar(Long id) {

    }

    private static Producto getProducto(ResultSet rs) throws SQLException {
        Producto p = new Producto();
        p.setId(rs.getLong("id"));
        p.setNombre(rs.getString("nombre"));
        p.setIdCategoria(rs.getLong("idCategoria"));
        p.setPrecio(rs.getDouble("precio"));
        p.setSku(rs.getString("sku"));
        p.setFecha_registro(rs.getDate("fecha_registro").toLocalDate());
        return p;
    }
}
