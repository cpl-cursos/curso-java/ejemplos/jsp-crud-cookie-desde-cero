package org.cpl_cursos.java.jsp.repositorios;

import org.cpl_cursos.java.jsp.modelos.Producto;

import java.util.List;

public interface Repositorio<T> {
    List<T> listar();
    T porId(Long id);
    void guardar(T t);
    void borrar(Long id);
}
